package team.huehue.br.pokeagenda;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class PokeStringGetRequest extends StringRequest {

    private Map<String, String> headers, params;

    public PokeStringGetRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.GET, url, listener, errorListener);
        headers = new HashMap<>();
        params = new HashMap<>();
    }

    public void addHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public void addParam(String key, String value) {
        this.params.put(key, value);
    }

    @Override
    protected Map<String, String> getParams() {
        return this.params;
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }
}
