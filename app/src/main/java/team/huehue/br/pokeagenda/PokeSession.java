package team.huehue.br.pokeagenda;

public class PokeSession {

    public final static PokeSession session = new PokeSession();

    private String login;
    private String senha;
    private String nome;
    private Long id;
    private Long favorito;
    private boolean admin;
    private PokeListItem[] pokemons;

    private PokeSession() {
        reset();
    }

    public void reset() {
        this.id = -1L;
        this.nome = null;
        this.senha = null;
        this.login = null;
        this.favorito = -1L;
        this.admin = false;
        this.pokemons = new PokeListItem[] {};
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFavorito() {
        return favorito;
    }

    public void setFavorito(Long favorito) {
        this.favorito = favorito;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public PokeListItem[] getPokemons() {
        return pokemons;
    }

    public void setPokemons(PokeListItem[] items) {
        this.pokemons = items;
    }

    public PokeListItem getPokemon(int pos) {
        return this.pokemons[pos];
    }
    @Override
    public String toString() {
        return "PokeSession{" +
                "login='" + login + '\'' +
                ", senha='" + senha + '\'' +
                ", nome='" + nome + '\'' +
                ", id=" + id +
                ", admin=" + admin +
                '}';
    }
}
