package team.huehue.br.pokeagenda;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Objects;

import static android.R.drawable.btn_star_big_on;
import static android.R.drawable.btn_star_big_off;

/**
 * Created by ana.peixoto on 17/06/2018.
 */

public class PokeDetailActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener {

    private final String REQUEST_TAG = "POKEBOOKMARK";
    private AlertDialog.Builder albuilder;
    private ProgressDialog pdlg;
    private AppVolleySingleton avs;
    private PokeStringGetRequest request;
    private PokeListItem pli;
    private Long pokeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pokeId = -1L;
        setContentView(R.layout.activity_poke_detail);
        albuilder = new AlertDialog.Builder(this);
        pdlg = new ProgressDialog(this);
        pdlg.setTitle("Aguarde...");
        avs = AppVolleySingleton.getInstance(getApplicationContext());
        Intent it = getIntent();
        if (it.getIntExtra("position", -1) > -1) {
            pli = PokeSession.session.getPokemon(it.getIntExtra("position", -1));
            ImageView iv = findViewById(R.id.pokePicture);
            EditText pokeName = findViewById(R.id.pokeName);
            EditText pokeSpecies = findViewById(R.id.pokeSpecies);
            EditText pokeWeight = findViewById(R.id.pokeWeight);
            EditText pokeHeight = findViewById(R.id.pokeHeight);
            EditText pokeOwner = findViewById(R.id.pokeOwner);
            iv.setImageBitmap(pli.getBitmap());
            pokeName.setText(pli.getNome());
            pokeSpecies.setText(pli.getEspecie());
            pokeOwner.setText(pli.getCriador());
            DecimalFormat fmt = new DecimalFormat("#0.00");
            pokeWeight.setText(fmt.format(pli.getPeso()));
            pokeHeight.setText(fmt.format(pli.getAltura()));
            pokeId = pli.getId();
            ImageButton IB = (ImageButton) findViewById(R.id.favImageView);
            if (PokeSession.session.getFavorito() == pokeId) {
                IB.setBackgroundResource(btn_star_big_on);
                IB.setImageResource(btn_star_big_on);
                IB.setTag("btn_star_big_on");
            } else {
                IB.setBackgroundResource(btn_star_big_off);
                IB.setImageResource(btn_star_big_off);
                IB.setTag("btn_star_big_off");
            }
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Mostrar o botão
        getSupportActionBar().setHomeButtonEnabled(true);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                finish();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }
    public void favoritar(View view) {
        pdlg.show();
        ImageButton IB = (ImageButton) findViewById(R.id.favImageView);
        if (IB.getTag().equals("btn_star_big_off")) {
            request = new PokeStringGetRequest(avs.buildEndpointURL("pokemon/" + pokeId + "/bookmark"), this, this);
        } else {
            request = new PokeStringGetRequest(avs.buildEndpointURL("pokemon/" + pokeId + "/unbookmark"), this, this);
        }
        request.addHeader("X-PokeAgenda-User", PokeSession.session.getLogin());
        request.addHeader("X-PokeAgenda-Password", PokeSession.session.getSenha());
        avs.addToRequestQueue(request, REQUEST_TAG);

    }

    @Override
    public void onStop() {
        super.onStop();
        avs.cancelPendingRequests(REQUEST_TAG);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResponse(String response) {
        Log.e("PokeAgenda", response);
        pdlg.dismiss();
        try {
            JSONObject resp = new JSONObject(response);
            JSONObject pokews = resp.getJSONObject("pokews-response");
            ImageButton IB = (ImageButton) findViewById(R.id.favImageView);
            if (pokews.getInt("code") == 201) {
                MainActivity.getInstance().setPokeFav(pli);
                IB.setBackgroundResource(btn_star_big_on);
                IB.setImageResource(btn_star_big_on);
                IB.setTag("btn_star_big_on");
                Toast.makeText(this, "O pokemon "+pli.getNome()+" é o seu favorito!", Toast.LENGTH_SHORT).show();
            } else if(pokews.getInt("code") == 202) {
                MainActivity.getInstance().setPokeFav(null);
                IB.setBackgroundResource(btn_star_big_off);
                IB.setImageResource(btn_star_big_off);
                IB.setTag("btn_star_big_off");
                Toast.makeText(this, "O pokemon "+pli.getNome()+" não é mais o seu favorito!", Toast.LENGTH_SHORT).show();
            } else {
                albuilder.setTitle("Erro").setMessage("Não foi possível favoritar/desfavoritar este pokemon, tente novamente.").setPositiveButton("OK", null).show();
            }
        } catch (JSONException jse) {
            albuilder.setTitle("Erro de comunicação!").setMessage("Resposta do servidor inválida").setPositiveButton("OK", null).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        pdlg.dismiss();
        albuilder.setTitle("Erro de comunicação!").setMessage("Ocorreu um erro de comunicação [" + error.getMessage() + "]").setPositiveButton("OK", null).show();
    }
}
