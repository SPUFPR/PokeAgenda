package team.huehue.br.pokeagenda;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class PokeStringPostRequest extends StringRequest {
    private Map<String, String> params;
    private Map<String, String> headers;

    public PokeStringPostRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, listener, errorListener);
        params = new HashMap<>();
        headers = new HashMap<>();
    }

    public void addParam(String key, String value) {
        this.params.put(key, value);
    }

    public void addHeader(String key, String value) {
        this.headers.put(key, value);
    }

    @Override
    protected Map<String, String> getParams() {
        return this.params;
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }

}
