package team.huehue.br.pokeagenda;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<String> {

    private static final String REQUEST_TAG = "POKEWELCOME";
    private static MainActivity instance;
    private Menu appMenu;
    private AlertDialog.Builder albuilder;
    private AppVolleySingleton avs;
    private PokeStringGetRequest request;
    private ProgressDialog pdlg;
    private TextView pokeFavNome;
    private ImageView pokeFavImage;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent it = getIntent();
        if (Intent.ACTION_SEARCH.equals(it.getAction())) {
            Log.i("PokeAgenda", "about to die");
            Intent searchIntent = new Intent(MainActivity.getInstance(), PokeListActivity.class);
            searchIntent.putExtra("search", 1);
            searchIntent.putExtra("query", it.getStringExtra(SearchManager.QUERY));
            startActivity(searchIntent);

            finish();
        } else {
            instance = this;
            setContentView(R.layout.activity_main);
            TextView tv = (TextView) findViewById(R.id.tv);
            pokeFavNome = findViewById(R.id.pokeFavNome);
            pokeFavImage = findViewById(R.id.pokefavImage);
            pokeFavNome.setVisibility(View.INVISIBLE);
            pokeFavImage.setVisibility(View.INVISIBLE);
            albuilder = new AlertDialog.Builder(this);
            pdlg = new ProgressDialog(this);
            pdlg.setTitle("Aguarde...");
            if (PokeSession.session.getId() >= 0) {
                tv.setText("Bem vindo " + PokeSession.session.getNome());
            } else {
                //mata o processo já.
                finish();
            }
            avs = AppVolleySingleton.getInstance(getApplicationContext());
            request = new PokeStringGetRequest(avs.buildEndpointURL("treinador/bookmarked"), this, this);
            request.setTag(REQUEST_TAG);
            request.addHeader("X-PokeAgenda-User", PokeSession.session.getLogin());
            request.addHeader("X-PokeAgenda-Password", PokeSession.session.getSenha());
            avs.addToRequestQueue(request, REQUEST_TAG);
            pdlg.show();
        }
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public void setPokeFav(PokeListItem itm) {
        if (itm != null) {
            pokeFavImage.setImageBitmap(Bitmap.createScaledBitmap(itm.getBitmap(), 500, 500, true));
            pokeFavNome.setText(itm.getNome());
            pokeFavNome.setVisibility(View.VISIBLE);
            pokeFavImage.setVisibility(View.VISIBLE);
            PokeSession.session.setFavorito(itm.getId());
        } else {
            pokeFavNome.setVisibility(View.INVISIBLE);
            pokeFavImage.setVisibility(View.INVISIBLE);
            PokeSession.session.setFavorito(-1L);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.appMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, appMenu);
        SearchManager sm = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView sv = (SearchView) appMenu.findItem(R.id.app_bar_search).getActionView();

        sv.setSearchableInfo(sm.getSearchableInfo(getComponentName()));
        sv.setIconified(false);
        sv.setIconifiedByDefault(false);
        //smenu.expandActionView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {

            case R.id.logout:
                finish();
                i = new Intent(this, LoginActivity.class);
                startActivity(i);
                break;
            case R.id.new_pokemon:
                //finish();
                i = new Intent(this, RegisterActivity.class);
                startActivity(i);
                break;
            case R.id.find_pokemon:
                // finish();
                /*i = new Intent(this, PokeListActivity.class);
                i.putExtra("search", 1);
                startActivity(i);*/
                MenuItem smenu = appMenu.findItem(R.id.app_bar_search);
                smenu.expandActionView();
                break;
            case R.id.app_bar_search:
                Log.i("PokeAgenda", "On search!");
                break;
            case R.id.list_pokemon:
                // finish();
                i = new Intent(this, PokeListActivity.class);
                startActivity(i);
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (avs != null)
            avs.cancelPendingRequests(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        pdlg.dismiss();
        albuilder.setTitle("Falha de Comunicação")
                .setMessage("Ocorreu um erro de comunicação [" + error.getMessage() + "]")
                .setPositiveButton("OK", null)
                .show();
    }

    @Override
    public void onResponse(String response) {
        pdlg.dismiss();
        try {
            Log.e("PokeAgenda", response);
            JSONObject resp = new JSONObject(response);
            JSONObject pokews = resp.getJSONObject("pokews-response");
            if (pokews.getInt("code") == 200) {
                JSONObject data = pokews.getJSONObject("data");
                PokeSession.session.setFavorito(data.getLong("id"));
                pokeFavNome.setText(data.getString("nome"));
                pokeFavNome.setVisibility(View.VISIBLE);
                new PokeImageLoader(avs.buildEndpointURL("pokemon/" + data.getLong("id") + "/picture"), pokeFavImage, avs).downloadImage();
            }
        } catch (JSONException jse) {
            albuilder.setTitle("Falha de comunicação!")
                    .setMessage("Resposta do servidor é inválida!")
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            this.appMenu.findItem(R.id.app_bar_search).collapseActionView();
        } catch(Exception ex) {}
    }
}
