package team.huehue.br.pokeagenda;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class AppVolleySingleton {

    private static String WS_URL = "http://pokeagenda.eaatech.com.br:8480/pokeagendaws";
    //private static String WS_URL = "http://10.0.2.2:8080/pokeagendaws";
    private static AppVolleySingleton instance;
    private static Context context;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private AppVolleySingleton(Context ctx) {
        context = ctx;
        getRequestQueue();
        imageLoader = new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(500);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                }
        );
    }

    public static synchronized AppVolleySingleton getInstance(Context context) {
        if (instance == null)
            instance = new AppVolleySingleton(context);
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return this.requestQueue;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public String buildEndpointURL(String endpoint) {
        return String.format("%s/%s", WS_URL, endpoint);
    }

}
