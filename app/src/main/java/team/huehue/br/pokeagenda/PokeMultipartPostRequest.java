package team.huehue.br.pokeagenda;

/**
 * Adaptado de https://gist.github.com/anggadarkprince/a7c536da091f4b26bb4abf2f92926594
 */

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class PokeMultipartPostRequest extends Request<NetworkResponse> {

    private final String doubleHiphens = "--";
    private final String lineEnd = "\r\n";
    private final String mimeBoundary = "pokeagenda-" + System.currentTimeMillis();

    private Response.Listener<NetworkResponse> listener;
    private Response.ErrorListener eListener;
    private Map<String, String> headers;
    private Map<String, String> params;
    private Map<String, PokeDataPart> parts;

    public PokeMultipartPostRequest(String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, errorListener);
        this.setRetryPolicy(new DefaultRetryPolicy(1000 * 60, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        this.setShouldCache(false);
        this.listener = listener;
        this.eListener = errorListener;
        this.headers = new HashMap<>();
        this.params = new HashMap<>();
        this.parts = new HashMap<>();
    }

    public void addHeader(String key, String val) {
        this.headers.put(key, val);
    }

    public void addParam(String key, String val) {
        this.params.put(key, val);
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Override
    public String getBodyContentType() {
        return "multipart/form-data;boundary=" + mimeBoundary;
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            if(params.size() > 0) {
                textParse(dos, params, getParamsEncoding());
            }
            if(parts.size() > 0) {
                dataParse(dos, parts);
            }
            dos.writeBytes(doubleHiphens + mimeBoundary + doubleHiphens + lineEnd);
            return bos.toByteArray();
        } catch(IOException ioe) {
        }
        return null;
    }

    public Map<String, PokeDataPart> getParts() {
        return this.parts;
    }

    private void textParse(DataOutputStream dataOutputStream, Map<String, String> params, String encoding) throws IOException {
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String val =  new String(entry.getValue().getBytes(encoding), "ISO-8859-1");
                buildTextPart(dataOutputStream, entry.getKey(), val);
            }
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + encoding, uee);
        }
    }

    private void buildTextPart(DataOutputStream dataOutputStream, String parameterName, String parameterValue) throws IOException {
        dataOutputStream.writeBytes(doubleHiphens + mimeBoundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + parameterName + "\"" + lineEnd);
        dataOutputStream.writeBytes(lineEnd);
        dataOutputStream.writeBytes(parameterValue + lineEnd);
    }


    private void dataParse(DataOutputStream dataOutputStream, Map<String, PokeDataPart> data) throws IOException {
        for (Map.Entry<String, PokeDataPart> entry : data.entrySet()) {
            buildDataPart(dataOutputStream, entry.getValue(), entry.getKey());
        }
    }

    private void buildDataPart(DataOutputStream dataOutputStream, PokeDataPart dataFile, String inputName) throws IOException {
        dataOutputStream.writeBytes(doubleHiphens + mimeBoundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" +
                inputName + "\"; filename=\"" + dataFile.getFileName() + "\"" + lineEnd);
        if (dataFile.getType() != null && !dataFile.getType().trim().isEmpty()) {
            dataOutputStream.writeBytes("Content-Type: " + dataFile.getType() + lineEnd);
        }
        dataOutputStream.writeBytes(lineEnd);

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(dataFile.getContent());
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        dataOutputStream.writeBytes(lineEnd);
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(
                    response,
                    HttpHeaderParser.parseCacheHeaders(response)
            );
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(NetworkResponse response) {
        listener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        eListener.onErrorResponse(error);
    }
}
