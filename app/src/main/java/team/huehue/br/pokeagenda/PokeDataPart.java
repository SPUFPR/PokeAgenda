package team.huehue.br.pokeagenda;

public class PokeDataPart {
    private String fileName;
    private byte[] content;
    private String type;

    public PokeDataPart(){
        this.fileName = System.currentTimeMillis() + ".png";
        this.type = "image/png";
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
