package team.huehue.br.pokeagenda;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * May required refactoring and list storage for itens
 */
public class ListCell extends ArrayAdapter<PokeListItem> {
    private final Activity context;
    private final PokeListItem[] pokemons;

    public ListCell(Activity context, PokeListItem[] items) {
        super(context, R.layout.linha, items);
        this.context = context;
        this.pokemons = items;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.linha, null, true);
        TextView txtTitle = rowView.findViewById(R.id.nomePokemon);
        TextView espTitle = rowView.findViewById(R.id.especiePokemon);
        ImageView imageView = rowView.findViewById(R.id.imgPokemon);
        txtTitle.setText(pokemons[position].getNome());
        espTitle.setText(pokemons[position].getEspecie());
        imageView.setImageBitmap(pokemons[position].getBitmap());
        return rowView;
    }

    public PokeListItem getPos(int pos) {
        return this.pokemons[pos];
    }

}
