package team.huehue.br.pokeagenda;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class RegisterActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<NetworkResponse> {

    private final String REQUEST_TAG = "POKEADD";
    private AlertDialog.Builder albuilder;
    private ProgressDialog pdlg;
    private AppVolleySingleton avs;
    private PokeMultipartPostRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        pdlg = new ProgressDialog(this);
        pdlg.setTitle("Aguarde...");
        avs = AppVolleySingleton.getInstance(getApplicationContext());
        albuilder = new AlertDialog.Builder(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Mostrar o botão
        getSupportActionBar().setHomeButtonEnabled(true);      //Ativar o botão
    }
    public void cancelar(View view){
       startActivity( new Intent(this,MainActivity.class));
    }
    public void imagem(View view){
        Intent intentPegaFoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intentPegaFoto, "Selecione uma imagem"), 123);
    }
    public void camera(View view){
        File file = new File(Environment.getExternalStorageDirectory() + "/arquivo.png");
        Uri outputFileUri = Uri.fromFile(file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(Intent.createChooser(intent, "Tire uma Foto"), 456);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String filePath = null;
        if(resultCode != Activity.RESULT_CANCELED){
            ImageView foto = (ImageView) findViewById(R.id.tst);
            if(requestCode == 123){
                Uri selectedImage = data.getData();

                if (selectedImage != null && "content".equals(selectedImage.getScheme())) {

                    Cursor cursor = this.getContentResolver().query(selectedImage, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA },
                            null, null, null);
                    cursor.moveToFirst();
                     filePath = cursor.getString(0);
                    cursor.close();
                }
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 250, 250, true);
                foto.setImageBitmap(bitmapReduzido);
                foto.setScaleType(ImageView.ScaleType.FIT_XY);
            } else
            if(requestCode == 456){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 3;
                Bitmap imageBitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/arquivo.png", options);
                Bitmap bitmapReduzido = Bitmap.createScaledBitmap(imageBitmap, 250, 250, true);
                //ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                //byte[] fotoBinario = outputStream.toByteArray();
               // String encodedImage = Base64.encodeToString(fotoBinario, Base64.DEFAULT);
                foto.setImageBitmap(bitmapReduzido);
                foto.setScaleType(ImageView.ScaleType.FIT_XY);

            }
        }
    }



    public void cadastrar(View view) {
        EditText name = findViewById(R.id.name);
        EditText species = findViewById(R.id.especie);
        EditText height = findViewById(R.id.altura);
        EditText weight = findViewById(R.id.peso);
        ImageView foto = findViewById(R.id.tst);
        if(name.length() == 0) {
            albuilder.setTitle("Nome é obrigatório!").setMessage("Preencha o nome do Pokemon").setPositiveButton("OK", null).show();
            return;
        }
        if(species.length() == 0) {
            albuilder.setTitle("Espécie é obrigatória!").setMessage("Preencha a espécie do Pokemon").setPositiveButton("OK", null).show();
            return;
        }
        if(height.length() == 0) {
            albuilder.setTitle("Altura é obrigatória!").setMessage("Preencha a altura do Pokemon").setPositiveButton("OK", null).show();
            return;
        }
        if(weight.length() == 0) {
            albuilder.setTitle("Peso é obrigatório!").setMessage("Preencha o peso do Pokemon").setPositiveButton("OK", null).show();
            return;
        }
        //não é nulo o drawable :(
        if(foto.getDrawable().getClass().getSimpleName().matches(".*VectorDrawableCompat")) {
            albuilder.setTitle("Foto é obrigatória!").setMessage("Selecione uma imagem ou use sua câmera").setPositiveButton("OK", null).show();
            return;
        }
        request = new PokeMultipartPostRequest(avs.buildEndpointURL("pokemon"), this, this);
        request.addHeader("X-PokeAgenda-User", PokeSession.session.getLogin());
        request.addHeader("X-PokeAgenda-Password", PokeSession.session.getSenha());
        request.addParam("pokemon.nome",name.getText().toString());
        request.addParam("pokemon.especie", species.getText().toString());
        request.addParam("pokemon.altura", height.getText().toString());
        request.addParam("pokemon.peso", weight.getText().toString());
        PokeDataPart pdp = new PokeDataPart();
        pdp.setContent(PokeImageHelper.getFileDataFromDrawable(getApplicationContext(), foto.getDrawable()));
        request.getParts().put("photo", pdp);
        avs.addToRequestQueue(request, REQUEST_TAG);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        avs.cancelPendingRequests(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        pdlg.dismiss();
        albuilder.setTitle("Erro de comunicação!")
                .setMessage(error.getMessage())
                .setPositiveButton("OK", null)
                .show();
    }

    @Override
    public void onResponse(NetworkResponse response) {
        pdlg.dismiss();
        String result = new String(response.data);
        try {
            Log.e("PokeAgenda", result);
            JSONObject resp = new JSONObject(result);
            JSONObject pokews = resp.getJSONObject("pokews-response");
            if(pokews.getInt("code") == 201) {
                albuilder.setTitle("Pokemon cadastrado com sucesso!").setMessage("Cadastro efetuado com sucesso.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        RegisterActivity.this.finish();
                    }
                }).show();
            } else {
                albuilder.setTitle("Falha ao cadastrar!").setMessage(pokews.getString("message")).setPositiveButton("OK",null).show();
            }
        } catch(JSONException jse) {
            albuilder.setTitle("Falha de comunicação!").setMessage("Ocorreu uma falha de comunicação, tente novamente.").setPositiveButton("OK", null).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                finish();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {

            case R.id.logout:
                finish();
                i = new Intent(this,LoginActivity.class);
                startActivity(i);
                return true;
            case R.id.list_pokemon:
                i = new Intent(this,PokeListActivity.class);
                startActivity(i);
                return true;
            case R.id.new_pokemon:
                //finish();
                i = new Intent(this,RegisterActivity.class);
                startActivity(i);
                return true;
            default: return false;
        }
    }*/

}
