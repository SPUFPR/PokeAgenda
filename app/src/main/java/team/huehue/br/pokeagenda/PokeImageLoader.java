package team.huehue.br.pokeagenda;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.Map;

public class PokeImageLoader implements ImageLoader.ImageListener {

    private String url;
    private ImageView imageView;
    private AppVolleySingleton volley;

    public PokeImageLoader(String url, ImageView destino, AppVolleySingleton volley) {
        this.url = url;
        this.imageView = destino;
        this.volley = volley;
        Log.e("PokeAgendaImageLoader", this.url);
    }

    public void downloadImage() {
        this.volley.getImageLoader().get(this.url, this);
    }

    @Override
    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
        if (response.getBitmap() != null && imageView != null) {
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(response.getBitmap(), 500, 500, true);
            imageView.setImageBitmap(bitmapReduzido);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //silence is golden sometimes....
        if (imageView != null)
            imageView.setVisibility(View.INVISIBLE);
    }
}
