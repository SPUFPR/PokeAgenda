package team.huehue.br.pokeagenda;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ana.peixoto on 05/06/2018.
 */

public class LoginActivity extends Activity implements Response.ErrorListener, Response.Listener<String> {
    private static final String REQUEST_TAG = "POKELOGIN";
    private AlertDialog.Builder albuilder;
    private ProgressDialog pdlg;
    private AppVolleySingleton avs;
    private PokeStringPostRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        albuilder = new AlertDialog.Builder(this);
        pdlg = new ProgressDialog(this);
        pdlg.setTitle("Aguarde....");
        pdlg.setCancelable(false);
        Log.e("PokeAgenda", PokeSession.session.toString());
        avs = AppVolleySingleton.getInstance(getApplicationContext());
        PokeSession.session.reset();
        checkMyPermissions();
    }

    public void logar(View view) throws JSONException {
        EditText user = (EditText) findViewById(R.id.login);
        EditText password = (EditText) findViewById(R.id.senha);
        if (user.length() == 0 || password.length() == 0) {
            albuilder
                    .setTitle("Falha no Login!")
                    .setMessage(String.format("Preencha todos os campos!"))
                    .setPositiveButton("OK", null)
                    .show();
            return;
        }
        PokeSession.session.setSenha(password.getText().toString());
        request = new PokeStringPostRequest(avs.buildEndpointURL("login"), this, this);
        request.setTag(REQUEST_TAG);
        request.addParam("login", user.getText().toString());
        request.addParam("senha", PokeSession.session.getSenha());
        AppVolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request, REQUEST_TAG);
        pdlg.show();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        avs.cancelPendingRequests(REQUEST_TAG);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        pdlg.dismiss();
        albuilder.setTitle("Erro de comunicação")
                .setMessage("Ocorreu um erro de comunicação [" + error.getMessage() + "]")
                .setPositiveButton("OK", null)
                .show();
    }

    @Override
    public void onResponse(String response) {
        pdlg.dismiss();
        try {
            Log.e("PokeAgenda", response);
            JSONObject resp = new JSONObject(response);
            JSONObject pokews = resp.getJSONObject("pokews-response");
            if (pokews.getInt("code") == 200) {
                Intent it = new Intent(this, MainActivity.class);
                JSONObject treinador = pokews.getJSONObject("data");
                PokeSession.session.setId(treinador.getLong("id"));
                PokeSession.session.setNome(treinador.getString("nome"));
                PokeSession.session.setLogin(treinador.getString("login"));
                PokeSession.session.setAdmin(treinador.getBoolean("admin"));
                startActivity(it);
                this.finish();
            } else {
                Log.e("PokeAgenda", "Falha no login");
                albuilder.setTitle("Falha no Login!")
                        .setMessage(pokews.getString("message"))
                        .setPositiveButton("OK", null)
                        .show();
            }
        } catch (JSONException jse) {
            Log.e("PokeAgenda","Exception", jse);
            albuilder.setTitle("Erro de comunicação!")
                    .setMessage("Resposta do servidor é inválida!")
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    private boolean checkMyPermissions() {
        String[] perms = {
                Manifest.permission.INTERNET,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };

        for(int i = 0; i < perms.length; ++i) {
            if(ActivityCompat.checkSelfPermission(this, perms[i]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, perms, 100);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int rcode, String perms[], int[] grants) {
        if (rcode == 100) {
            if (grants.length == 0) {
                Log.i("PokeAgenda", "Usuário negou permissões");
                return;
            }
            boolean canIgo = true;
            for (int i = 0; i < grants.length; ++i) {

                if (grants[i] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("PokeAgenda", "Permissão " + perms[i] + " negada pelo usuário");
                    canIgo = false;
                }
            }
            if (!canIgo) {
                Log.e("PokeAgenda", "Fechando aplicação : sem permissões");
                finish();
            } else {
                Log.i("PokeAgenda","Temos o que precisamos!");
            }
        }
    }

}
