package team.huehue.br.pokeagenda;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PokeListActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<String> {

    public ListView lista;
    private AlertDialog.Builder albuilder;
    private StringRequest request;
    private ProgressDialog pdlg, waitdlg;
    private AppVolleySingleton avs;
    private final String REQUEST_TAG = "POKELISTSEARCH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poke_list);
        albuilder = new AlertDialog.Builder(this);
        pdlg = new ProgressDialog(this);
        pdlg.setCancelable(false);
        pdlg.setProgress(0);
        pdlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        waitdlg = new ProgressDialog(this);
        waitdlg.setCancelable(false);
        waitdlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        waitdlg.setTitle("Aguarde...");

        avs = AppVolleySingleton.getInstance(getApplicationContext());
        lista = findViewById(android.R.id.list);
        lista.setAdapter(null);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(PokeListActivity.this, PokeDetailActivity.class);
                i.putExtra("position", position);
                startActivity(i);
            }
        });
        Intent it = getIntent();
        if (it.getIntExtra("search", -1) > -1) {
            String query = it.getStringExtra("query");
            request = new PokeStringPostRequest(avs.buildEndpointURL("pokemon/search"), this, this);
            ((PokeStringPostRequest) request).addParam("nome", query);
            ((PokeStringPostRequest) request).addHeader("X-PokeAgenda-User", PokeSession.session.getLogin());
            ((PokeStringPostRequest) request).addHeader("X-PokeAgenda-Password", PokeSession.session.getSenha());
        } else {
            request = new PokeStringGetRequest(avs.buildEndpointURL("pokemon"), this, this);
            ((PokeStringGetRequest) request).addHeader("X-PokeAgenda-User", PokeSession.session.getLogin());
            ((PokeStringGetRequest) request).addHeader("X-PokeAgenda-Password", PokeSession.session.getSenha());
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Mostrar o botão
        getSupportActionBar().setHomeButtonEnabled(true);
        waitdlg.show();
        avs.addToRequestQueue(request, REQUEST_TAG);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        avs.cancelPendingRequests(REQUEST_TAG);
    }

    @Override
    public void onResponse(String response) {
        Log.i("PokeAgenda", response);
        waitdlg.dismiss();
        try {
            JSONObject resp = new JSONObject(response);
            JSONObject pokews = resp.getJSONObject("pokews-response");
            if (pokews.getInt("code") == 200) {
                JSONArray data = pokews.getJSONArray("data");
                if (data.length() > 0) {
                    final PokeListItem[] items = new PokeListItem[data.length()];
                    final ProgressDialog myPdlg = pdlg;
                    pdlg.setMax(data.length());
                    pdlg.setTitle("Carregando dados...");
                    pdlg.show();
                    for (int i = 0; i < data.length(); ++i) {
                        final PokeListItem itm = new PokeListItem();
                        final ListView myList = lista;
                        JSONObject objItm = data.getJSONObject(i);
                        itm.setId(objItm.getLong("id"));
                        itm.setNome(objItm.getString("nome"));
                        itm.setEspecie(objItm.getString("especie"));
                        itm.setAltura(objItm.getDouble("altura"));
                        itm.setPeso(objItm.getDouble("peso"));
                        itm.setCriador(objItm.getString("criador"));
                        items[i] = itm;
                        //image loading
                        new PokeImageLoader(avs.buildEndpointURL("pokemon/" + itm.getId() + "/picture"), null, avs) {
                            @Override
                            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                if (response.getBitmap() != null) {
                                    itm.setBitmap(response.getBitmap());
                                    myPdlg.incrementProgressBy(1);
                                    if (myPdlg.getProgress() == myPdlg.getMax()) {
                                        myPdlg.dismiss();
                                        ListCell adapter = new ListCell(PokeListActivity.this, items);
                                        PokeSession.session.setPokemons(items);
                                        myList.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                myPdlg.incrementProgressBy(1);
                                if (myPdlg.getProgress() == myPdlg.getMax()) {
                                    myPdlg.dismiss();
                                    ListCell adapter = new ListCell(PokeListActivity.this, items);
                                    PokeSession.session.setPokemons(items);
                                    myList.setAdapter(adapter);
                                }
                            }
                        }.downloadImage();

                    }
                } else {
                    pdlg.dismiss();
                    Toast.makeText(this, "Nenhum pokémon encontrado!", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException jse) {
            pdlg.dismiss();
            PokeSession.session.setPokemons(null);
            albuilder.setTitle("Falha de comunicação!")
                    .setMessage("Resposta do servidor é inválida!")
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        waitdlg.dismiss();
        PokeSession.session.setPokemons(null);
        albuilder.setTitle("Falha de Comunicação")
                .setMessage("Ocorreu um erro de comunicação [" + error.getMessage() + "]")
                .setPositiveButton("OK", null)
                .show();
    }

}
